# huebuild

Changes your Philips Hue lights to match the build status of a CI project.

Works with GitLab CI and Travis CI.

## Requirements

* Python 3

## Setup

```
git clone https://gitlab.com/ianunruh/huebuild.git && cd huebuild
pip install -r requirements.txt
```

## Usage

Before you can use `huebuild`, you need to setup the configuration file.

```
cp config.sample.yml config.yml
vim config.yml
```

Then just start it up.

```
./huebuild.py
```

You can place this process under a supervisor or init system to keep it running in the background.

You can use `./huebuild.py --help` to find out more options.
