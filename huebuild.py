#!/usr/bin/env python
from argparse import ArgumentParser
import logging
import time
import yaml

from gitlab import Gitlab
from phue import Bridge
import requests


class Color:
    RED = 'red'
    YELLOW = 'yellow'
    GREEN = 'green'
    TEAL = 'teal'
    BLUE = 'blue'
    PURPLE = 'purple'


class TravisPlugin:
    def __init__(self, config):
        self.repo = config['repo']

    def get_latest_status(self):
        resp = requests.get('https://api.travis-ci.org/repos/{}/builds'.format(self.repo))
        resp.raise_for_status()
        data = resp.json()
        return data[0]['state']

    def color_for_build_status(self, status):
        if status in ['created', 'pending']:
            return Color.TEAL
        elif status == 'running':
            return Color.BLUE
        elif status == 'failed':
            return Color.RED

        return Color.GREEN


class GitlabPlugin:
    def __init__(self, config):
        self.client = Gitlab(config.get('url', 'https://gitlab.com'), config.get('token'))
        self.project = self.client.projects.get(config['project'])

    def get_latest_status(self):
        return self.project.pipelines.list()[0].status

    def color_for_build_status(self, status):
        if status in ['created', 'pending']:
            return Color.TEAL
        elif status == 'running':
            return Color.BLUE
        elif status == 'failed':
            return Color.RED

        return Color.GREEN


CI_PLUGINS = {
    'gitlab': GitlabPlugin,
    'travis': TravisPlugin,
}


def load_config(path):
    with open(path) as fp:
        return yaml.load(fp)


def get_ci_plugin(config):
    return CI_PLUGINS[config['plugin']](config)


def main():
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', default='config.yml',
                        help='Path to config file [config.yml]')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Log more verbosely')

    args = parser.parse_args()

    config = load_config(args.config)

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    bridge = Bridge(config['hue_bridge_ip'])

    ci_plugin = get_ci_plugin(config['ci'])

    current_status = None
    while True:
        latest_status = ci_plugin.get_latest_status()
        if latest_status == current_status:
            logging.debug('Build status unchanged since last poll [%s]', current_status)
        else:
            logging.info('Build status changed [%s] [%s]', current_status, latest_status)
            current_status = latest_status

        color = ci_plugin.color_for_build_status(current_status)
        hue = config['colors'][color]

        command = config['hue_command'].copy()
        command['hue'] = hue
        bridge.set_light(config['hue_lights'], command)

        try:
            time.sleep(config['poll_interval'])
        except KeyboardInterrupt:
            break


main()
